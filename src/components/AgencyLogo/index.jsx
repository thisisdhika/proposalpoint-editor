import * as React from 'react';
import propType from 'prop-types';
import clsx from 'clsx';

// Style Module
import style from './AgencyLogo.module.scss';

const AgencyLogo = ({ text, as, className }) => {
  const El = as || 'span';

  return (
    <El data-testid="AgencyLogo" className={clsx(style.agencyLogo, className)}>
      {text}
    </El>
  );
};

AgencyLogo.propsType = {
  text: propType.string,
  as: propType.oneOfType([
    propType.element,
    propType.elementType,
    propType.string,
  ]),
};

AgencyLogo.defaultProps = {
  text: 'YOUR AGENCY',
};

export default AgencyLogo;
