import * as React from 'react';
import { render } from '@testing-library/react';
import AgencyLogo from '.';

describe('Test Component', () => {
  let props;

  beforeEach(() => {
    props = {
      text: 'HAHAGENCY',
    };
  });

  const renderComponent = () => render(<AgencyLogo {...props} />);

  it('should render text text correctly', () => {
    const { getByTestId } = renderComponent();
    const component = getByTestId('AgencyLogo');
    expect(component).toHaveTextContent('HAHAGENCY');
  });
});
