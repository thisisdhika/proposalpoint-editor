import * as React from "react";
import { render } from "@testing-library/react";
import Header from ".";

describe("Test Component", () => {
  let props;

  beforeEach(() => {
    props = {
      foo: "bar"
    };
  });

  const renderComponent = () => render(<Header {...props} />);

  it("should render foo text correctly", () => {
    props.foo = "harvey was here";
    const { getByTestId } = renderComponent();
    const component = getByTestId("Header");
    expect(component).toHaveTextContent("harvey was here");
  });
});
