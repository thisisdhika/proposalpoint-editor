import * as React from 'react';
import { render } from '@testing-library/react';
import Sidebar from '.';

describe('Test Component', () => {
  let props;

  beforeEach(() => {
    props = {
      foo: 'bar',
    };
  });

  const renderComponent = () => render(<Sidebar {...props} />);

  it('should render foo text correctly', () => {
    props.foo = 'harvey was here';
    const { getByTestId } = renderComponent();
    const component = getByTestId('Sidebar');
    expect(component).toHaveTextContent('harvey was here');
  });
});
