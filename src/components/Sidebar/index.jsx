/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/anchor-has-content */
import * as React from 'react';
// import propType from 'prop-types';
import clsx from 'clsx';
import AgencyLogo from '../AgencyLogo';

// Style Module
import style from './Sidebar.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons';

const menuItems = [
  {
    label: 'Welcome',
    href: '#welcome',
  },
  {
    label: 'Our Difference',
    href: '#our-difference',
  },
  {
    label: 'Your Property',
    href: '#your-property',
  },
  {
    label: 'Suburb Sales Stats',
    href: '#suburb-sales-stats',
  },
  {
    label: 'Market Trends',
    href: '#market-trends',
  },
  {
    label: 'Peak Selling Periods',
    href: '#peak-selling-periods',
  },
  {
    label: 'Property Characteristics',
    href: '#property-characteristics',
  },
  {
    label: 'Demographics',
    href: '#demographics',
  },
  {
    label: 'Marketing',
    href: '#marketing',
  },
  {
    label: 'Property Value',
    href: '#property-value',
  },
  {
    label: 'Your Sales Team',
    href: '#your-sales-team',
  },
  {
    label: 'Testimonials',
    href: '#testimonials',
  },
  {
    label: 'Our Team Sales',
    href: '#our-team-sales',
  },
  {
    label: 'Next Steps',
    href: '#next-steps',
  },
];

const Sidebar = ({ ...restProps }) => (
  <aside data-testid="Sidebar" className={clsx(style.sidebar)} {...restProps}>
    <header className={clsx(style['sidebar--header'])}>
      <a href="#" className={clsx(style['sidebar--header--brand'])}>
        <AgencyLogo />
      </a>
    </header>
    <section className={clsx(style['sidebar--body'])}>
      <div className={clsx(style['sidebar--body--inner'])}>
        <ul className={clsx('list-unstyled', style['sidebar--body--menu'])}>
          {menuItems.map(({ label, href }) => (
            <li key={href}>
              <a href={href}>{label}</a>
            </li>
          ))}
        </ul>
        <div className={clsx(style['sidebar--body--info'])}>
          <h1>John Smith</h1>
          <p>Sales Agent</p>
          <p>john.smith@youragency.com</p>
          <ul
            className={clsx(
              'list-unstyled',
              style['sidebar--body--info--links']
            )}
          >
            <li>
              <a href="#">
                <FontAwesomeIcon icon={faInstagram} />
              </a>
            </li>
            <li>
              <a href="#">
                <FontAwesomeIcon icon={faFacebookF} />
              </a>
            </li>
            <li>
              <a href="#">
                <FontAwesomeIcon icon={faLinkedinIn} />
              </a>
            </li>
            <li>
              <a href="#">
                <FontAwesomeIcon icon={faYoutube} />
              </a>
            </li>
          </ul>
        </div>
        <div className={clsx(style['sidebar--body--info'])}>
          <h1>Your Agency</h1>
          <p>14 Ocean St. Narrabeen. NSW</p>
          <a href="//www.proposalpoint.com.au" className={clsx('text-coral')}>
            www.proposalpoint.com.au
          </a>
        </div>
      </div>
    </section>
  </aside>
);

Sidebar.propsType = {};

Sidebar.defaultProps = {};

export default Sidebar;
