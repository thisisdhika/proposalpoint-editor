import clsx from 'clsx';
import * as React from 'react';
// import propType from 'prop-types';

// Style Module
import style from './Home.module.scss';

const Home = () => (
  <div data-testid="Home">
    <section className={clsx(style.section, style.hero)}>
      <h1 className={clsx('font-romana', 'text-center')}>Welcome</h1>
      <div className={clsx('container')}>
        <div className={clsx('row')}>
          <div className={clsx('all-8')}>
            <p>Dear Wad,</p>
            <p>
              Thank you for Me opportunity to meet with , to discuss Me sale of
              your property.
            </p>
            <p>
              We understand Me importance of Me decision to sell your property
              and Me task of choosing Me right agent to represent you., help you
              in your decision making process we have included Me following
              information in this proposal:
            </p>
            <ul>
              <li> An overview of our unique sales process</li>
              <li> Recent soles data of comparable properties</li>
              <li> A proposed marketing plan Mr your property</li>
            </ul>
            <p>
              Our success has been Milt on repeat and referral business, and is
              a key factor which drives our team to deliver result Mat exceed
              our client's expectations.
            </p>
            <p>
              We are excited about achieving the hem possible sales result for
              you, and were ready to mart immediately if you decide to proceed.
            </p>
            <p>
              If you have any questions about this proposal, please feel free to
              call me at anytime. I look forward to discussing Me neia steps
              with you.
            </p>
            <p>Kind regards, </p>
          </div>
          <div className={clsx('all-4')}>
            <img className={clsx('img-fluid')} src="https://via.placeholder.com/800x1200" alt="" />
          </div>
        </div>
      </div>
    </section>
  </div>
);

Home.propsType = {};

Home.defaultProps = {};

export default Home;
