import * as React from "react";
import { render } from "@testing-library/react";
import Home from ".";

describe("Test Component", () => {
  let props;

  beforeEach(() => {
    props = {
      foo: "bar"
    };
  });

  const renderComponent = () => render(<Home {...props} />);

  it("should render foo text correctly", () => {
    props.foo = "harvey was here";
    const { getByTestId } = renderComponent();
    const component = getByTestId("Home");
    expect(component).toHaveTextContent("harvey was here");
  });
});
