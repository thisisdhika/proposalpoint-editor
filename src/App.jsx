import clsx from 'clsx';
import * as React from 'react';
import { Sidebar } from './components';
import { Home } from './pages';

class App extends React.Component {
  render() {
    return (
      <>
        {/* Header Component Goes Here */}
        <main className={clsx('main-content')}>
          <Home />
        </main>
        <Sidebar />
      </>
    );
  }
}

export default App;
