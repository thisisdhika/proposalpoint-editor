module.exports = componentName => ({
  content: `import * as React from "react";
import propType from 'prop-types';

// Style Module
import style from "./${componentName}.module.scss";

const ${componentName} = ({ foo }) => (
    <div data-testid="${componentName}">{foo}</div>
);

${componentName}.propsType = {}

${componentName}.defaultProps = {}

export default ${componentName};
`,
  extension: `.jsx`,
  name: 'index',
});
