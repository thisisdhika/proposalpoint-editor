require('colors');
const fs = require('fs');
const templates = require('./templates');

const componentName = process.argv[2];

if (!componentName) {
  console.error('Invalid Component Name'.red);
  process.exit(1);
}

console.log('Creating Component Templates with name: ' + componentName);

const componentDirectory = `./src/components/${componentName}`;

if (fs.existsSync(componentDirectory)) {
  console.error(`Component ${componentName} already exists.`.red);
  process.exit(1);
}

fs.mkdirSync(componentDirectory);

const generatedTemplates = templates.map(template => template(componentName));

generatedTemplates.forEach(template => {
  fs.writeFileSync(
    `${componentDirectory}/${
      Object.prototype.hasOwnProperty.call(template, 'name')
        ? template.name
        : componentName
    }${template.extension}`,
    template.content
  );
});

console.log(
  'Successfully created component under: ' + componentDirectory.green
);
